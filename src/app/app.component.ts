import { Component } from '@angular/core';
import { CompanyForm } from './CompanyForm';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'my-app';
  companyForm=new CompanyForm();
  
  // Register(regForm:any){
  //     console.log(regForm);

  //     let fname=regForm.controls.firstname.value;
  //     let lname=regForm.controls.lastname.value;
  //     let email=regForm.controls.email.value;
  //     let course=regForm.controls.course.value;

  //     console.log("First Name : "+fname+"<br>");
  //     console.log("Last Name : "+lname+"<br>");
  //     console.log("Email : "+email+"<br>");
  //     console.log("Course : "+course+"<br>");
  // }

  companyRegister(form:NgForm){

    if (form.invalid) {
      return;
    }
    console.log(form.controls['company'].value);
    console.log(form.controls['descriptionForCompany'].value);
    console.log(form.controls['compId'].value);
    console.log(form.controls['projects'].value);
  }
}
